<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function index()
	{
		redirect('siteman/index','refresh');
	}

	public function get_alat()
	{
		$kode = $this->input->get('kode');
		$q = $this->model_app->view_where('data_alat',array('kode'=>$kode));
		$alat = $q->row_array();
		$data = array(
            'sn'      =>  $alat['sn'],
            'nama_alat'   =>  htmlentities($alat['nama_alat']),
            'merk'    =>  htmlentities($alat['merk']),
            'model_tipe'    =>  htmlentities($alat['model_tipe']),
            'lokasi'    =>  htmlentities($alat['lokasi']),
        );
 		echo json_encode($data);
	}

}

/* End of file Ajax.php */
/* Location: ./application/controllers/Ajax.php */