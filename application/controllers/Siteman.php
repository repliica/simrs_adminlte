<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siteman extends CI_Controller {

	function index()
	{
		
		if (isset($_POST['submit'])) {
			$username = $this->input->post('a');
			$password = md5($this->input->post('b'));
			// echo $password;
			$cek = $this->model_app->cek_login($username, $password,'user');
			$row = $cek->row_array();
			$total = $cek->num_rows();
			if ($total > 0) {
				$array = array(
					'username'=> $row['username'],
					'level'   => $row['level'],
					'id_users'=> $row['id'],
					'nama' 	  => $row['nama']
				);
				
				$this->session->set_userdata( $array );
				redirect('siteman/home');
			}else{
				$data['title'] = 'Username atau Password salah!';
				$this->load->view('siteman/admin-login',$data);
			}
		}else{
			if ($this->session->userdata('level')=='admin') {
				 redirect('siteman/home','refresh');
			}else{
				$this->load->view('siteman/admin-login');
			}
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('siteman');
	}

	function home()
	{
		cek_session_admin();
		$data['title'] = "Dashboard admin";
		$this->template->load('siteman/template','siteman/home',$data);
	}

	function scan_qr()
	{
		cek_session_admin();
		$data['title'] = "Scan QR code";
		$this->template->load('siteman/template','siteman/scanQR',$data);
	}

	function inventaris()
	{
		cek_session_admin();
		$data['title'] = 'Daftar alat kesehatan';
		$data['record'] = $this->model_app->view_ordering('data_alat','id','DESC');
		$this->template->load('siteman/template','siteman/mod_inventaris/inv',$data);
	}

	function tambah_inventaris()
	{
		cek_session_admin();
		if (isset($_POST['submit'])) {
			$config['upload_path'] = 'assets/uploads/';
			$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
			$config['max_size']  = '2000'; //Kb
			$config['encrypt_name'] = true;
			$this->load->library('upload', $config);
			$this->upload->do_upload('k');
			$hasil = $this->upload->data();

			$nama_alat = $this->db->escape_str($this->input->post('c'));

			if ($nama_alat=="Infrared Therapy") {
				$kode = "IR";
			}elseif ($nama_alat=="Ultrasound Therapy") {
				$kode = "UST";
			}elseif ($nama_alat=="Microwave Diathermy") {
				$kode = "MWD";
			}elseif ($nama_alat=="Shortwave Diathermy") {
				$kode = "SWD";
			}elseif ($nama_alat=="Electrostimulator") {
				$kode = "TENS";
			}elseif ($nama_alat=="Static Bycicle") {
				$kode = "GYM";
			}elseif ($nama_alat=="Nebulizer") {
				$kode = "NB";
			}elseif ($nama_alat=="Traksi") {
				$kode = "TRK";
			}elseif ($nama_alat=="Laser") {
				$kode = "LSR";
			}elseif ($nama_alat=="Parafin Bath") {
				$kode = "PRF";
			}else{
				$kode = "UM";
			}
			$kd = $this->mylibrary->kode_auto('data_alat','id',$kode,$kode);
			$nom = preg_replace("/[^0-9]/", "",$kd);

			// buat QR dari kode alat
			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
	        $config['cacheable']    = true; //boolean, the default is true
	        $config['cachedir']     = './assets/'; //string, the default is application/cache/
	        $config['errorlog']     = './assets/'; //string, the default is application/logs/
	        $config['imagedir']     = './assets/scanQR/'; //direktori penyimpanan qr code
	        $config['quality']      = true; //boolean, the default is true
	        $config['size']         = '1024'; //interger, the default is 1024
	        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
	        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
	        $this->ciqrcode->initialize($config);
	 
	        $image_name=md5($kd).'.png'; //buat name dari qr code sesuai dengan nim
	 
	        $params['data'] = $kd; //data yang akan di jadikan QR CODE
	        $params['level'] = 'H'; //H=High
	        $params['size'] = 10;
	        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
	        $this->ciqrcode->generate($params);
			if ($hasil['file_name']!='') {
				$data = array(
						'kode' => $kd,
						'no' => $nom,
						'sn' => $this->db->escape_str($this->input->post('b')),
						'nama_alat' => $nama_alat,
						'merk' => $this->db->escape_str($this->input->post('d')),
						'model_tipe' => $this->db->escape_str($this->input->post('e')),
						'tahun_pgd' => $this->db->escape_str($this->input->post('f')),
						'tahun_opr' => $this->db->escape_str($this->input->post('g')),
						'distributor' => $this->db->escape_str($this->input->post('h')),
						'lokasi' => $this->db->escape_str($this->input->post('i')),
						'status_klbs' => $this->db->escape_str($this->input->post('j')),	
						'status_klb' => $hasil['file_name'],
						'qrcode' => $image_name
					);
			}else{
				$data = array(
						'kode' => $kd,
						'no' => $nom,
						'sn' => $this->db->escape_str($this->input->post('b')),
						'nama_alat' => $nama_alat,
						'merk' => $this->db->escape_str($this->input->post('d')),
						'model_tipe' => $this->db->escape_str($this->input->post('e')),
						'tahun_pgd' => $this->db->escape_str($this->input->post('f')),
						'tahun_opr' => $this->db->escape_str($this->input->post('g')),
						'distributor' => $this->db->escape_str($this->input->post('h')),
						'lokasi' => $this->db->escape_str($this->input->post('i')),
						'status_klbs' => $this->db->escape_str($this->input->post('j')),
						'qrcode' => $image_name
					);
			}
			$q = $this->model_app->insert('data_alat',$data);
			if ($q) {
				redirect('siteman/inventaris/berhasil','refresh');
			}else{
				redirect('siteman/inventaris/gagal','refresh');
			}

		}else{
			cek_session_admin();
			$data['title'] = 'Tambah Data Alat kesehatan';
			$this->template->load('siteman/template','siteman/mod_inventaris/tambah_inventaris',$data);
		}
	}

	function ubah_inventaris()
	{
		cek_session_admin();
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])) {
			$config['upload_path'] = 'assets/uploads/';
			$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
			$config['max_size']  = '2000'; //Kb
			$config['encrypt_name'] = true;
			$this->load->library('upload', $config);
			$this->upload->do_upload('k');
			$hasil = $this->upload->data();



			$nama_alat = $this->db->escape_str($this->input->post('c'));

			if ($nama_alat=="Infrared Therapy") {
				$kode = "IR";
			}elseif ($nama_alat=="Ultrasound Therapy") {
				$kode = "UST";
			}elseif ($nama_alat=="Microwave Diathermy") {
				$kode = "MWD";
			}elseif ($nama_alat=="Shortwave Diathermy") {
				$kode = "SWD";
			}elseif ($nama_alat=="Electrostimulator") {
				$kode = "TENS";
			}elseif ($nama_alat=="Static Bycicle") {
				$kode = "GYM";
			}elseif ($nama_alat=="Nebulizer") {
				$kode = "NB";
			}elseif ($nama_alat=="Traksi") {
				$kode = "TRK";
			}elseif ($nama_alat=="Laser") {
				$kode = "LSR";
			}elseif ($nama_alat=="Parafin Bath") {
				$kode = "PRF";
			}else{
				$kode = "UM";
			}
			$kd = $this->mylibrary->kode_auto('data_alat','id',$kode,$kode);
			$nom = preg_replace("/[^0-9]/", "",$kd);

			// buat QR dari kode alat
			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
	        $config['cacheable']    = true; //boolean, the default is true
	        $config['cachedir']     = './assets/'; //string, the default is application/cache/
	        $config['errorlog']     = './assets/'; //string, the default is application/logs/
	        $config['imagedir']     = './assets/scanQR/'; //direktori penyimpanan qr code
	        $config['quality']      = true; //boolean, the default is true
	        $config['size']         = '1024'; //interger, the default is 1024
	        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
	        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
	        $this->ciqrcode->initialize($config);
	 
	        $image_name=md5($kd).'.png'; //buat name dari qr code sesuai dengan nim
	 
	        $params['data'] = $kd; //data yang akan di jadikan QR CODE
	        $params['level'] = 'H'; //H=High
	        $params['size'] = 10;
	        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        	$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
			if ($hasil['file_name']!='') {
				$data = array(
						'kode' => $kd,
						'no' => $nom,
						'sn' => $this->db->escape_str($this->input->post('b')),
						'nama_alat' => $nama_alat,
						'merk' => $this->db->escape_str($this->input->post('d')),
						'model_tipe' => $this->db->escape_str($this->input->post('e')),
						'tahun_pgd' => $this->db->escape_str($this->input->post('f')),
						'tahun_opr' => $this->db->escape_str($this->input->post('g')),
						'distributor' => $this->db->escape_str($this->input->post('h')),
						'lokasi' => $this->db->escape_str($this->input->post('i')),
						'status_klbs' => $this->db->escape_str($this->input->post('j')),	
						'status_klb' => $hasil['file_name'],
						'qrcode' => $image_name
					);
			}else{
				$data = array(
						'kode' => $kd,
						'no' => $nom,
						'sn' => $this->db->escape_str($this->input->post('b')),
						'nama_alat' => $nama_alat,
						'merk' => $this->db->escape_str($this->input->post('d')),
						'model_tipe' => $this->db->escape_str($this->input->post('e')),
						'tahun_pgd' => $this->db->escape_str($this->input->post('f')),
						'tahun_opr' => $this->db->escape_str($this->input->post('g')),
						'distributor' => $this->db->escape_str($this->input->post('h')),
						'lokasi' => $this->db->escape_str($this->input->post('i')),
						'status_klbs' => $this->db->escape_str($this->input->post('j')),
						'qrcode' => $image_name
					);
			}
			
			$where = array('id'=>$this->input->post('id'));
			$q = $this->model_app->update('data_alat',$data,$where);
			if ($q) {
				redirect('siteman/inventaris/berhasil','refresh');
			}else{
				redirect('siteman/inventaris/gagal','refresh');
			}

		}else{
			cek_session_admin();
			$data['title'] = 'Ubah Data Alat kesehatan';
			$data['row'] = $this->model_app->edit('data_alat',array('id'=>$id))->row_array();
			$this->template->load('siteman/template','siteman/mod_inventaris/ubah_inventaris',$data);
		}
	}

	function hapus_inventaris()
	{
		cek_session_admin();
		$data = array('id'=>$this->uri->segment(3));
		$this->model_app->delete('data_alat',$data);
		redirect('siteman/inventaris');
	}

	function download($file)
	{
		$this->load->helper('download');
		force_download('assets/uploads/'.$file , NULL);
	}

	function jadwal()
	{
		cek_session_admin();
		$data['title'] = 'Jadwal Pemeliharaan';
		$data['record'] = $this->model_app->view_ordering('jadwal_pm','id','DESC');
		$this->template->load('siteman/template','siteman/mod_jadwal/jadwal',$data);

	}

	function tambah_jadwal()
	{
		if (isset($_POST['submit'])) {

			$kode = $this->input->post('kode');
			$sn = htmlspecialchars($this->input->post('sn'));
			$nama_alat = htmlspecialchars($this->input->post('nama_alat'));
			$merk = htmlspecialchars($this->input->post('merk'));
			$model_tipe = htmlspecialchars($this->input->post('model_tipe'));
			$lokasi = htmlspecialchars($this->input->post('lokasi'));
			$jenis_pm = htmlspecialchars($this->input->post('jenis_pm'));
			$tgl_pm = htmlspecialchars($this->input->post('tgl_pm'));
			$tgl_pm2 = date('Y-m-d', strtotime('+6 months', strtotime($this->input->post('tgl_pm'))));
			$tgl_pm3 = date('Y-m-d', strtotime('+12 months', strtotime($this->input->post('tgl_pm'))));

			$data = array(
				'kode' => $kode,
				'jenis_pm' => $jenis_pm,
				'tgl_pm' => $tgl_pm,
				'tgl_pm2' => $tgl_pm2,
				'tgl_pm3' => $tgl_pm3,
				'nama_alat' => $nama_alat,
				'merk' => $merk,
				'model_tipe' => $model_tipe,
				'lokasi' => $lokasi,
				'sn' => $sn,
			);
			$q = $this->model_app->insert('jadwal_pm',$data);
			if ($q) {
				redirect('siteman/jadwal/berhasil','refresh');
			}else{
				redirect('siteman/jadwal/gagal','refresh');
			}
		}else{
			$data['title'] = 'Tambah Jadwal Pemeliharaan';
			$data['record'] = $this->model_app->view_ordering('data_alat','id','DESC');
			$this->template->load('siteman/template','siteman/mod_jadwal/tambah_jadwal',$data);
		}
	}

	function update_jadwal()
	{
		cek_session_admin();
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])) {

			$kode = $this->input->post('kode');
			$sn = htmlspecialchars($this->input->post('sn'));
			$nama_alat = htmlspecialchars($this->input->post('nama_alat'));
			$merk = htmlspecialchars($this->input->post('merk'));
			$model_tipe = htmlspecialchars($this->input->post('model_tipe'));
			$lokasi = htmlspecialchars($this->input->post('lokasi'));
			$jenis_pm = htmlspecialchars($this->input->post('jenis_pm'));
			$jadwal_pm = htmlspecialchars($this->input->post('jadwal_pm'));
			$tgl_pm = htmlspecialchars(date('Y-m-d', strtotime($this->input->post('tgl_pm'))));
			$status = htmlspecialchars($this->input->post('status'));
			$masalah = htmlspecialchars($this->input->post('masalah'));
			$analisis = htmlspecialchars($this->input->post('analisis'));
			$tindakan = htmlspecialchars($this->input->post('tindakan'));

			$data = array(
				'kode' => $kode,
				'jenis_pm' => $jenis_pm,
				'jadwal_pm' => $jadwal_pm,
				'tgl_pm' => $tgl_pm,
				'status' => $status,
				'masalah' => $masalah,
				'analisis' => $analisis,
				'tindakan' => $tindakan,
				'sn' => $sn,
				'nama_alat' => $nama_alat,
				'merk' => $merk,
				'model_tipe' => $model_tipe,
				'lokasi' => $lokasi,
			);

			$up = date('Y-m-d', strtotime('+6 months', strtotime($tgl_pm)));
			$up2 = date('Y-m-d', strtotime('+12 months', strtotime($tgl_pm)));
			$up3 = date('Y-m-d', strtotime('+18 months', strtotime($tgl_pm)));

		 	$datas = array(
		 	 	'tgl_pm' => $up,
		 	 	'tgl_pm2' => $up2,
		 	 	'tgl_pm3' => $up3
		 	);

		 	$this->model_app->update('jadwal_pm',$datas,array('id'=>$id));

			$q = $this->model_app->insert('data_pm',$data);
			if ($q) {
				redirect('siteman/jadwal/berhasil','refresh');
			}else{
				redirect('siteman/jadwal/gagal','refresh');
			}
		}else{
			$data['title'] = 'Tambah Jadwal Pemeliharaan';
			$data['row'] = $this->model_app->edit('jadwal_pm',array('id'=>$id))->row_array();
			$this->template->load('siteman/template','siteman/mod_jadwal/update_jadwal',$data);
		}
	}

	function ubah_jadwal()
	{
		// cek_session_admin();
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])) {

			$where = array('id'=>$this->input->post('id'));
			$jenis_pm = htmlspecialchars($this->input->post('jenis_pm'));
			$tgl_pm = htmlspecialchars($this->input->post('tgl_pm'));
			$tgl_pm2 = date('Y-m-d', strtotime('+6 months', strtotime($this->input->post('tgl_pm'))));
			$tgl_pm3 = date('Y-m-d', strtotime('+12 months', strtotime($this->input->post('tgl_pm'))));

			$data = array(
				'jenis_pm' => $jenis_pm,
				'tgl_pm' => $tgl_pm,
				'tgl_pm2' => $tgl_pm2,
				'tgl_pm3' => $tgl_pm3,
			);
			$q = $this->model_app->update('jadwal_pm',$data,$where);
			if ($q) {
				redirect('siteman/jadwal/berhasil','refresh');
			}else{
				redirect('siteman/jadwal/gagal','refresh');
			}
		}else{
			$data['title'] = 'Tambah Jadwal Pemeliharaan';
			$data['row'] = $this->model_app->edit('jadwal_pm',array('id'=>$id))->row_array();
			$this->template->load('siteman/template','siteman/mod_jadwal/ubah_jadwal',$data);
		}
	}

	function hapus_jadwal()
	{
		cek_session_admin();
		$data = array('id'=>$this->uri->segment(3));
		$this->model_app->delete('jadwal_pm',$data);
		redirect('siteman/jadwal');
	}

	function data()
	{
		$data['title'] = 'Data Pemeliharaan';
		$data['inventaris'] = $this->model_app->view_ordering('data_pm','jadwal_pm','DESC');
		$this->template->load('siteman/template','siteman/mod_data/data',$data);
	}

	function ubah_data()
	{
		cek_session_admin();
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])) {

			$tgl_pm = htmlspecialchars(date('Y-m-d', strtotime($this->input->post('tgl_pm'))));
			$status = htmlspecialchars($this->input->post('status'));
			$masalah = htmlspecialchars($this->input->post('masalah'));
			$analisis = htmlspecialchars($this->input->post('analisis'));
			$tindakan = htmlspecialchars($this->input->post('tindakan'));

			$data = array(
				'tgl_pm' => $tgl_pm,
				'status' => $status,
				'masalah' => $masalah,
				'analisis' => $analisis,
				'tindakan' => $tindakan,
			);

			$q = $this->model_app->insert('data_pm',$data);
			if ($q) {
				redirect('siteman/jadwal/berhasil','refresh');
			}else{
				redirect('siteman/jadwal/gagal','refresh');
			}
		}else{
			$data['title'] = 'Ubah data Pemeliharaan';
			$data['row'] = $this->model_app->edit('data_pm',array('id'=>$id))->row_array();
			$this->template->load('siteman/template','siteman/mod_data/update_data',$data);
		}
	}

	function detil_data()
	{
		cek_session_admin();
		$id = $this->uri->segment(3);
		$data['alt'] = $this->model_app->view_where('data_pm',array('id'=>$id))->row_array();
		$this->load->view('siteman/mod_data/detil',$data);
		
	}

	function riwayat_data()
	{
		cek_session_admin();
		$id = $this->uri->segment(3);
		$data['alt']= $this->model_app->view_where('data_pm',array('id'=>$id))->row_array();
		$this->load->view('siteman/mod_data/riwayat',$data);
		
	}

	function hapus_data()
	{
		cek_session_admin();
		$data = array('id'=>$this->uri->segment(3));
		$this->model_app->delete('data_pm',$data);
		redirect('siteman/data');
	}

	// pdf akses fpdf
	function indexPdf(){
		$this->load->library('pdf');
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(190,7,'SEKOLAH MENENGAH KEJURUSAN NEEGRI 2 LANGSA',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'DAFTAR SISWA KELAS IX JURUSAN REKAYASA PERANGKAT LUNAK',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,6,'NIM',1,0);
        $pdf->Cell(85,6,'NAMA MAHASISWA',1,0);
        $pdf->Cell(27,6,'NO HP',1,0);
        $pdf->Cell(25,6,'TANGGAL LHR',1,1);
        $pdf->SetFont('Arial','',10);
        $mahasiswa = $this->db->get('data_alat')->result();
        foreach ($mahasiswa as $row){
            $pdf->Cell(20,6,$row->kode,1,0);
            $pdf->Cell(85,6,$row->nama_alat,1,0);
            $pdf->Cell(27,6,$row->merk,1,0);
            $pdf->Cell(25,6,$row->tahun_pgd,1,1); 
        }
        $pdf->Output();
    }


	

}

/* End of file Siteman.php */
/* Location: ./application/controllers/Siteman.php */