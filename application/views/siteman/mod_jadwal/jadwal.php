<style>
.dataTable > thead > tr > th[class*="sort"]:before,
.dataTable > thead > tr > th[class*="sort"]:after {
    content: "" !important;
}
</style>
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h2><?=$title;?></h2>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active"><?= ucwords(str_replace("_"," ", $this->uri->segment('2'))) ?></li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">      
    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Semua Data</h3>
              <div style="float:right;" class="mb-2">
                <a href="<?=base_url().'siteman/tambah_jadwal'?>" class="btn btn-primary btn-sm pull-right">Tambah Data</a>    
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="dataTables-example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Kode Inventaris</th>
                    <th>S/N</th>
                    <th>Nama Alat</th>
                    <th>Merk</th>
                    <th>Model/Tipe</th>
                    <th>Lokasi Alat</th>
                    <th>Jenis</th>
                    <th>Jadwal 1</th>
                    <th>Jadwal 2</th>
                    <th>Jadwal 3</th>
                    <th><i class="fa fa-list"></i></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($record as $row) { ?>  
                      <tr style="background-color:#f9c7c7;">
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row["kode"]; ?></td> 
                        <td><?php echo $row["sn"]; ?></td>
                        <td><?php echo $row["nama_alat"]; ?></td>
                        <td><?php echo $row["merk"]; ?></td>
                        <td><?php echo $row["model_tipe"]; ?></td>
                        <td><?php echo $row["lokasi"]; ?></td>
                        <td><?php echo $row["jenis_pm"]; ?></td>
                        <td><?php echo $this->mylibrary->tgl_indo($row['tgl_pm']); ?></td>
                        <td><?php 
                        if ($row['nama_alat']=='Infrared Therapy' OR
                            $row['nama_alat']=='Nebulizer'OR
                            $row['nama_alat']=='Shortwave Diathermy'
                        ) {
                        echo $this->mylibrary->tgl_indo($row['tgl_pm2']);}
                        else {
                        echo '-';
                        }
                          ?>
                      </td>
                        <td><?php
                        echo $this->mylibrary->tgl_indo($row['tgl_pm']);
                          ?>
                        </td>    
                        <td style="text-align: center">
                          <a href="<?=base_url().'siteman/ubah_jadwal/'.$row['id']?>">
                            <i class="fa fa-cog" title="Edit data"></i>
                          </a><br>
                          <a href="<?=base_url().'siteman/update_jadwal/'.$row['id']?>" title='Update data'>
                            <i class="fas fa-book"></i>
                          </a><br>
                          <a href="<?=base_url().'siteman/hapus_jadwal/'.$row['id']?>"
                          onclick="return confirm('apakah anda yakin akan menghapus data ini?')";>
                            <i class="fa fa-trash" style="color: red;" title="hapus data"></i>
                          </a>
                        </td>
                      </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
    </div>
  </div>
</section>
