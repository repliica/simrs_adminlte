<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SimRs</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets_adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <style type="text/css">
        .thumbnail {
            border: 0;
        }

        #webcodecam-canvas, #scanned-img {
            background-color: #2d2d2d;
        }

        #camera-select {
            display: inline-block;
            width: auto;
        }

        .btn {
            margin-bottom: 2px;
        }

        .form-control {
            height: 32px;
        }

        .h4, h4 {
            width: auto;
            float: left;
            font-size: 20px;
            line-height: 1.1;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .controls {
            float: right;
            display: inline-block;
        }

        .well {
            position: relative;
            display: inline-block;
        }

        .panel-heading {
            display: inline-block;
            width: 100%;
        }

        .container {
            width: 100%
        }

        pre {
            border: 0;
            border-radius: 0;
            background-color: #333;
            margin: 0;
            line-height: 125%;
            color: whitesmoke;
        }

        button {
            outline: none !important;
        }

        .table-bordered {
            color: #777;
            cursor: default;
        }

        .table-bordered a:hover {
            text-decoration: none;
        }

        .table-bordered th a {
            float: right;
            line-height: 3.49;
        }

        .table-bordered td a {
            float: left;
        }

        .table-bordered th img {
            float: left;
        }

        .table-bordered th, .table-bordered td {
            vertical-align: middle !important;
        }

        .scanner-laser {
            position: absolute;
            margin: 40px;
            height: 30px;
            width: 30px;
            opacity: 0.5;
        }

        .laser-leftTop {
            top: 0;
            left: 0;
            border-top: solid red 5px;
            border-left: solid red 5px;
        }

        .laser-leftBottom {
            bottom: 0;
            left: 0;
            border-bottom: solid red 5px;
            border-left: solid red 5px;
        }

        .laser-rightTop {
            top: 0;
            right: 0;
            border-top: solid red 5px;
            border-right: solid red 5px;
        }

        .laser-rightBottom {
            bottom: 0;
            right: 0;
            border-bottom: solid red 5px;
            border-right: solid red 5px;
            JS
        }

        #webcodecam-canvas {
            background-color: #272822;
        }
        #scanned-QR{
            word-break: break-word;
        }
    </style>
</head>

<body class="<?=base_url()?>assets_adminlte/hold-transition sidebar-mini layout-fixed">
<div id="alldata" class="wrapper">

  
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <?php include 'header.php'; ?>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include 'side-nav.php'; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php echo $contents; ?>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="<?=base_url()?>">Politeknik Kesehatan Jakarta</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?=base_url()?>assets_adminlte/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url()?>assets_adminlte/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=base_url()?>assets_adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?=base_url()?>assets_adminlte/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?=base_url()?>assets_adminlte/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?=base_url()?>assets_adminlte/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?=base_url()?>assets_adminlte/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=base_url()?>assets_adminlte/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=base_url()?>assets_adminlte/plugins/moment/moment.min.js"></script>
<script src="<?=base_url()?>assets_adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=base_url()?>assets_adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?=base_url()?>assets_adminlte/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url()?>assets_adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets_adminlte/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url()?>assets_adminlte/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets_adminlte/dist/js/demo.js"></script>
<!-- DATA TABLE SCRIPTS -->
<!-- DataTables -->
<script src="<?=base_url()?>assets_adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets_adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets_adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=base_url()?>assets_adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script>
  $('#loader').show();
  $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "scrollX": true,
      "paging": true,
      "info" : true,
      "bDestroy" : true,
      "searching" :true,      
    });
  });
</script>
<!-- CUSTOM SCRIPTS -->
<script src="<?=base_url()?>assets/js/custom.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/filereader.js"></script>
 <!-- Using jquery version: -->
 <!--
 <script type="text/javascript" src="js/jquery.js"></script>
 <script type="text/javascript" src="js/qrcodelib.js"></script>
 <script type="text/javascript" src="js/webcodecamjquery.js"></script>
 <script type="text/javascript" src="js/mainjquery.js"></script>
 -->
 <script type="text/javascript" src="<?=base_url()?>assets/js/qrcodelib.js"></script>
 <script type="text/javascript" src="<?=base_url()?>assets/js/webcodecamjs.js"></script>
 <script type="text/javascript" src="<?=base_url()?>assets/js/main.js"></script>
</body>
</html>
