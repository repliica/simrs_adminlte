<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h2><?=$title;?></h2>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active"><?= ucwords(str_replace("_"," ", $this->uri->segment('2'))) ?></li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">      
    <div class="row">
      <div class="col-md-12">
          <div class="card" id="QR-Code">
            <div class="card-header" style="background-color: #d9edf7">
              <div class="row">
                <!-- <div class="col-md-12"> -->
                    <div class="col-md-5">
                        <select class="form-control" style="width: 100%" id="camera-select"></select>
                    </div>
                    <input id="image-url" type="text" class="form-control col-md-4" placeholder="Image url">
                    <div class="form-group col-md-3" style="padding-top: 3px">
                        <button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="fa fa-upload"></span></button>
                        <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="fa fa-image"></span></button>
                        <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="fa fa-play"></span></button>
                        <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="fa fa-pause"></span></button>
                        <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="fa fa-stop"></span></button>
                    </div>
                <!-- </div> -->
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="panel-body text-center row">
                    <div class="col-md-6 card">
                        <div class="card-body">
                            <div class="well" style="position: relative;display: inline-block;">
                                <canvas width="320" height="240" id="webcodecam-canvas"></canvas>
                                <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="well row card mb-0" style="width: 100%; padding-top: 20px; padding-bottom: 20px">
                            <label id="zoom-value" class="col-md-12" width="100">Zoom: 2</label>
                            <div class="col-md-12">
                                <input id="zoom"  onchange="Page.changeZoom();" type="range" min="10" max="30" value="20">
                            </div>
                            <label id="brightness-value" class="col-md-12" width="100">Brightness: 0</label>
                            <div class="col-md-12">
                                <input id="brightness" onchange="Page.changeBrightness();" type="range" min="0" max="128" value="0">
                            </div>
                            <label class="col-md-12" id="contrast-value" width="100">Contrast: 0</label>
                            <div class="col-md-12">
                                <input id="contrast" onchange="Page.changeContrast();" type="range" min="0" max="64" value="0">
                            </div>
                            <label class="col-md-12" id="threshold-value" width="100">Threshold: 0</label>
                            <div class="col-md-12">
                                <input id="threshold" onchange="Page.changeThreshold();" type="range" min="0" max="512" value="0">
                            </div>
                            <label id="sharpness-value" width="100">Sharpness: off</label>
                            <input id="sharpness" onchange="Page.changeSharpness();" type="checkbox"> |
                            <label id="grayscale-value" width="100">grayscale: off</label>
                            <input id="grayscale" onchange="Page.changeGrayscale();" type="checkbox">
                            <br>
                            <label id="flipVertical-value" width="100">Flip Vertical: off</label>
                            <input id="flipVertical" onchange="Page.changeVertical();" type="checkbox"> |
                            <label id="flipHorizontal-value" width="100">Flip Horizontal: off</label>
                            <input id="flipHorizontal" onchange="Page.changeHorizontal();" type="checkbox">
                        </div>
                        <br><br><br>
                    </div>
                    <div class="col-md-12">
                        <div class="thumbnail card" id="result" style="padding-top: 20px">
                            <div class="well" style="overflow: hidden;">
                                <img width="320" height="240" id="scanned-img" src="">
                            </div>
                            <div class="caption">
                                <h3>Scanned result</h3>
                                <p id="scanned-QR"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
    </div>
  </div>
</section>