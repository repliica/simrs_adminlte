<!-- Brand Logo -->
    <a href="<?= base_url() ?>" class="brand-link">
      <img src="<?=base_url()?>assets_adminlte/dist/img/AdminLTELogo2.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
      style="width:40px; height: 4em">
      <span class="brand-text font-weight-light">SimRs</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?=base_url()?>assets_adminlte/dist/img/avatar5.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $this->session->userdata('nama') ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <a href="<?=base_url()?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?=base_url().'siteman/inventaris'?>" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                 Data Alat
                <i class="fas fa-angle-right right"></i>
                <span class="badge badge-info right">6</span>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?=base_url().'siteman/jadwal'?>" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Jadwal Pemeliharaan
                <i class="fas fa-angle-right right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?=base_url().'siteman/data'?>" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
              Data Pemeliharaan
              <i class="fas fa-angle-right right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
              MMEL
              <i class="fas fa-angle-right right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tree"></i>
              <p>
              Supplier
              <i class="fas fa-angle-right right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?=base_url().'siteman/scan_qr'?>" class="nav-link">
              <i class="nav-icon far fa-calendar"></i>
              <p>
              Laporan Kerusakan
              <i class="fas fa-angle-right right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?=base_url().'siteman/scan_qr'?>" class="nav-link">
              <i class="nav-icon fas fa-qrcode"></i>
              <p>
              Scan QR
              <i class="fas fa-angle-right right"></i>
              </p>
            </a>
          </li>
          <li class="nav-header">Extras</li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Dokumentasi Sistem
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                About Me
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->