<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h2><?=$title;?></h2>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active"><?= ucwords(str_replace("_"," ", $this->uri->segment('2'))) ?></li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">      
    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Form Tambah Data</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <?php $attributes = array('class'=>'form-horizontal','role'=>'form','autocomplete'=>'off');
                echo form_open_multipart('siteman/update_data',$attributes); ?>
                    <div class='col-md-12'>
                      <table class='table table-condensed table-bordered'>
                      <tbody>
                        <input type="hidden" name="id" value="<?=$row['id'];?>">
                        <tr><th scope='row'>Kode</th><td>            <input type='text' class='form-control' name='kode' readonly id="kode" value="<?=$row['kode']?>"> </td></tr>
                        <tr><th scope='row'>SN</th><td>              <input type='text' class='form-control' name='sn' readonly id="sn" value="<?=$row['sn']?>"> </td></tr>
                        <tr><th scope='row'>Nama Alat</th><td>       <input type="text" name="nama_alat" class="form-control" readonly id="nmalat" value="<?=$row['nama_alat']?>"> </td></tr>
                        <tr><th scope='row'>Merk</th><td>            <input type='text' class='form-control' name='merk' readonly id="merk" value="<?=$row['merk']?>"> </td></tr>
                        <tr><th scope='row'>Model Tipe</th><td>      <input type='text' class='form-control' name='model_tipe' readonly id="model" value="<?=$row['model_tipe']?>"> </td></tr>
                         <tr><th scope='row'>Lokasi</th><td>         <input type="text" class='form-control' name="lokasi" readonly id="lokasi" value="<?=$row['lokasi']?>"> </td></tr>
                        <tr><th scope='row'>Jenis Pemeliharaan</th><td> <input type="text" class='form-control' name="jenis_pm" readonly id="jenis_pm" value="<?=$row['jenis_pm']?>"></td></tr>

                        <tr><th scope='row'>Jadwal Pemeliharaan</th><td><input type='date' class='form-control' name='jadwal_pm' value="<?=$row['jadwal_pm'];?>" readonly> </td></tr>
                        <tr><th scope='row'>Tanggal Pelaksanaan</th><td><input type='date' class='form-control' name='tgl_pm' value="<?=$row['tgl_pm'];?>"> </td></tr>
                        <tr><th scope='row'>Status</th><td> <select name="status" class="form-control">
                                                              <option value="<?=$row['status'];?>"><?=$row['status'];?></option>
                                                              <option value="Selesai">Selesai</option>
                                                              <option value="Pending">Pending</option>
                                                            </select> </td></tr>
                        <tr><th scope='row'>Masalah</th><td> <textarea class="form-control" name="masalah" placeholder="isikan masalah"><?=$row['masalah'];?></textarea></td></tr>

                        <tr><th scope='row'>Analisis</th><td> <textarea class="form-control" name="analisis" placeholder="isikan analisis"><?=$row['analisis'];?></textarea></td></tr>

                        <tr><th scope='row'>Tindakan</th><td> <textarea class="form-control" name="tindakan" placeholder="isikan tindakan"><?=$row['tindakan'];?></textarea></td></tr>
                        
                      </tbody>
                      </table>

                      <div class='box-footer'>
                            <button type='submit' name='submit' class='btn btn-info'>Ubah</button>
                            <a href='<?=base_url().'siteman/data'?>' class='btn btn-default pull-right'>Kembali</a>
                      </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.card-body -->
            <script type="text/javascript">
                function isi_otomatis(){
                    var kode = $("#kode").val();
                    $.ajax({
                        url: '<?=base_url().'ajax/get_alat'?>',
                        data:"kode="+kode,
                        success: function(data){
                          var json = data,
                          obj = JSON.parse(json);
                          $('#sn').val(obj.sn);
                          $('#nmalat').val(obj.nama_alat);
                          $('#merk').val(obj.merk);
                          $('#model').val(obj.model_tipe);
                          $('#lokasi').val(obj.lokasi);
                        }
                    })
                }
            </script>
          </div>
          <!-- /.card -->
      </div>
    </div>
  </div>
</section>