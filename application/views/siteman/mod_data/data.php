<style>
.borderless td, .borderless th {
    border: none;
}
.table-condensed>thead>tr>th, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>tbody>tr>td, .table-condensed>tfoot>tr>td{
    padding: 0px;
}
.table-not-striped tbody tr:nth-of-type(odd) { 
  background-color: transparent!important; 
}
.dataTable > thead > tr > th[class*="sort"]:before,
.dataTable > thead > tr > th[class*="sort"]:after {
    content: "" !important;
}
</style>
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h2><?=$title;?></h2>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active"><?= ucwords(str_replace("_"," ", $this->uri->segment('2'))) ?></li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">      
    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Semua Data</h3>
              <div style="float:right;" class="mb-2">
                <a href="<?=base_url().'siteman/tambah_jadwal'?>" class="btn btn-primary btn-sm pull-right">Tambah Data</a>    
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="dataTables-example" class="table table-bordered table-striped" style="width: 100%">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Alat</th>
                    <th>Status</th>
                    <th>Masalah</th>
                    <th>Anls Kerusakan</th>
                    <th>Tindak Lanjut</th>
                    <th>Service Report</th>
                    <th><i class="fa fa-list"></i></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=1; ?>
                  <?php   
                  //looping array
                    foreach ($inventaris as $row) {
                  ?>  
                      <tr <?php if ($row['status']=='Pending') {?>
                        style="background-color:#f9c7c7;"
                        <?php } ?>>
                        <td><?php   echo $i; ?></td>
                        <td> 
                        <table class="table borderless table-condensed table-not-striped">
                          <tr><th  style="width: 70px">Kode </th><td>:</td><td>&nbsp;<a href="<?=base_url().'siteman/riwayat_data/'.$row['id']?>"><?php   echo $row["kode"]; ?></a> </td></tr>
                          <tr><th style="width: 70px">Nama </th><td>:</td><td> <?php   echo $row["nama_alat"]; ?></td></tr>
                          <tr><th style="width: 70px">S/N </th><td>:</td><td> <?php   echo $row["sn"]; ?></td></tr>
                          <tr><th style="width: 70px">Merk </th><td>:</td><td> <?php   echo $row["merk"]; ?></td></tr>
                          <tr><th style="width: 70px">Model </th><td>:</td><td> <?php   echo $row["model_tipe"]; ?></td></tr>
                          <tr><th style="width: 70px">Lokasi </th><td>:</td><td> <?php   echo $row["lokasi"]; ?></td></tr>
                          <tr><th style="width: 70px">Jenis </th><td>:</td><td> <?php   echo $row["jenis_pm"];?></td></tr>
                          <tr><th style="width: 70px">Jwl.Pem </th><td>:</td><td> <?php   echo $this->mylibrary->tgl_indo($row["jadwal_pm"]);?></td></tr>
                          <tr><th style="width: 70px">Tgl.Pel </th><td>:</td><td> <?php   echo $this->mylibrary->tgl_indo($row["tgl_pm"]);?></td></tr>
                        </table>
                        </td>
                        <td><?php   echo $row["status"]; ?></td>
                        <td><?php   echo $row["analisis"]; ?></td>
                        <td><?php   echo $row["masalah"]; ?></td>
                        <td><?php   echo $row["tindakan"]; ?></td>
                        <td><a href="<?=base_url().'siteman/detil_data/'.$row['id']?>">view report</a></td>
                        <td style="text-align: center">
                          <a href="<?=base_url().'siteman/ubah_data/'.$row['id']?>">
                            <i class="fa fa-cog"></i>
                          </a>
                          <a href="<?=base_url().'siteman/hapus_data/'.$row['id']?>"><i class="fa fa-trash" style="color: red;" onclick="return confirm('apakah anda yakin akan menghapus data ini?')";></i></a>
                            
                          </a>
                        </td>
                      </tr>
                <?php $i++; ?>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
    </div>
  </div>
</section>
