<style>
.dataTable > thead > tr > th[class*="sort"]:before,
.dataTable > thead > tr > th[class*="sort"]:after {
    content: "" !important;
}
</style>
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h2><?=$title;?></h2>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active"><?= ucwords(str_replace("_"," ", $this->uri->segment('2'))) ?></li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">      
    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Semua Data</h3>
              <div style="float:right;" class="mb-2">
                <a href="<?=base_url().'siteman/tambah_inventaris'?>" class="btn btn-primary btn-sm pull-right">Tambah Data</a>    
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <table id="dataTables-example" class="table table-bordered table-striped">
                <thead>
                  <tr role="row">
                    <th>No.</th>
                    <th>Kode Inventaris</th>
                    <th>S/N</th>
                    <th>Nama Alat</th>
                    <th>Merk</th>
                    <th>Model/Tipe</th>
                    <th>Tahun Pengadaan</th>
                    <th>Tahun Operasional</th>
                    <th>Distributor</th>
                    <th>Lokasi Alat</th>
                    <th>Status Kalibrasi</th>
                    <th><i class="fa fa-list"></i></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($record as $row) { ?>  
                      <tr class="gradeA odd"
                    <?php if ($row['status_klbs']=='Tidak Laik') {?>
                        style="background-color:#f9c7c7;"
                        <?php } ?>>
                        <td><?php   echo $no; ?></td>
                        <td><?php echo $row["kode"] ?></td>
                        <td><?php   echo $row["sn"]; ?></td>
                        <td><?php   echo $row["nama_alat"]; ?></td>
                        <td><?php   echo $row["merk"]; ?></td>
                        <td><?php   echo $row["model_tipe"]; ?></td>
                        <td><?php   echo $row["tahun_pgd"]; ?></td>
                        <td><?php   echo $row["tahun_opr"]; ?></td>
                        <td><?php   echo $row["distributor"]; ?></td>
                        <td><?php   echo $row["lokasi"]; ?></td>
                        <td style="text-align: center">
                          <?php   echo $row["status_klbs"]; ?>
                          <br>
                          <a href="<?=base_url().'siteman/download/'.$row["status_klb"]?>" target="_blank">lihat file</a> 
                        </td>
                        <td style="text-align: center">
                          <a href="<?=base_url().'siteman/ubah_inventaris/'.$row['id']?>">
                            <i class="fa fa-cog"></i>
                          </a>
                          <a href="<?=base_url().'siteman/hapus_inventaris/'.$row['id']?>"
                          onclick="return confirm('apakah anda yakin akan menghapus data ini?')";>
                            <i class="fa fa-trash" style="color: red;"></i>
                          </a>
                        </td>
                      </tr>
                  <?php   $no++; ?>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
    </div>
  </div>
</section>
